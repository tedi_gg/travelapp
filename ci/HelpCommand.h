#pragma once

#include "Command.h"

using namespace std;

class HelpCommand :
	public Command
{

	static void print(Command*, void*);

public:
	HelpCommand() : Command("help") {}
//	HelpCommand(const SaveCommand& cmd) : Command(cmd) {}
//	~HelpCommand() {};

	void help(ostream&);
	void execute(istream&, ostream&, string&, string&, Context&);
};


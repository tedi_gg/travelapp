
//#include <iostream>

#include "CommandInterpreter.h"
#include "OpenCommand.h"
#include "CloseCommand.h"
#include "SaveCommand.h"
#include "SaveasCommand.h"
#include "HelpCommand.h"
#include "ExitCommand.h"
#include "Context.h"

//void methods:
void CommandInterpreter::remove()
{
	if (orderedCommands != nullptr) {
		for (vector<Command*>::iterator it = orderedCommands->begin(); it.operator!=(orderedCommands->end()); it++) {
			Command* c = *it;
			if (c == nullptr) {
				break;
			}
			delete c;
		}

		delete orderedCommands;
	}
	if (commands != nullptr) {
		commands->clear();
		delete commands;
	}

}

void CommandInterpreter::copy(const CommandInterpreter& other)
{
	this->commands = new map<string, Command*>();
	this->orderedCommands = new vector<Command*>;
	// TODO
	commands = other.commands;
	for (vector<Command*>::iterator it = other.orderedCommands->begin(); it < other.orderedCommands->end(); it++) {
		Command* c = *it;
		addCommand(c);
	}
	for (map<string, Command*>::iterator it = other.commands->begin(); it != other.commands->end(); it++) {
		Command* c = it->second;
		addCommandImpl(it->first,c);
	}
}

//Constructors:
CommandInterpreter::CommandInterpreter(OpenHandler* oHandler)
{
	this->commands = new map<string,Command*>;
	this->orderedCommands = new vector<Command*>;
	this->oHandler = oHandler;

	// add default commands
	if (oHandler != nullptr) {
		addCommandImpl("open", new OpenCommand());
		addCommandImpl("close", new CloseCommand());
		addCommandImpl("save", new SaveCommand());
		addCommandImpl("saveas", new SaveasCommand());
	}

	addCommandImpl("help", new HelpCommand());
	addCommandImpl("exit", new ExitCommand());
}

CommandInterpreter::CommandInterpreter(const CommandInterpreter& other)
{
	if (this != &other) {
		copy(other);
	}

}

CommandInterpreter& CommandInterpreter::operator=(const CommandInterpreter& other)
{
	if (this != &other) {
		remove();
		copy(other);
	}

	return *this;
}

CommandInterpreter::~CommandInterpreter(){
	remove();
}

void CommandInterpreter::addCommandImpl(const string& name, Command* cmd) {
	commands->insert(std::pair<string, Command*>(name, cmd));
	if ((*commands)[name] == nullptr) {
		(*commands)[name] = cmd;
	}
	orderedCommands->push_back(cmd);
}

bool CommandInterpreter::addCommand(Command* cmd){
	string name = cmd->getName();
	if ((*commands)[name] != nullptr) {
		return false;
	}
	addCommandImpl(name, cmd);
	return true;
}
void CommandInterpreter::mainLoop(File* file, OpenHandler* oHandler) {

	Context ctx(this);
	if (file != nullptr && oHandler != nullptr) {
		CIObject* ciObject = oHandler->open(*file);
		ctx.doOpen(*file, ciObject);
	}
	else if(file != nullptr || oHandler != nullptr) {
		cout << "Error" << endl;
		return;
	}
	char* line = new char[4096];

	while (!ctx.isExit()) {
		cout << ">";
		cin.getline(line, 4096);

		int len = strlen(line);
		if (len == 0) {
			continue;
		}

		int p = 0;
		for (; p < len; p++) {
			if (line[p] == ' ') {
				break;
			}
		}

		if (p < len) {
			line[p] = 0;
		}

		string strCmd = line;

		Command* cmd;
		
		if (ctx.isSupportExtentsCmdList()) {
			CIObject* ciObj = ctx.getObject();
			if (ciObj->isNeedToBuildCmdList()) {
				ctx.getObject()->build(new VIterator<Command*>(orderedCommands));
			}

			cmd = ciObj->getCommand(strCmd);
		}
		else {
			cmd = (*commands)[strCmd];
		}
		if (cmd == nullptr) {
			// print that command not cound
			cout << "Command not found [" << strCmd << "]" << endl;
			for (map<string, Command*>::iterator it = commands->begin(); it != commands->end(); it++) {
				cout << it->first << " -> " << it->second << endl;
			}
			continue;
		}

		string args = (p < len) ? line + (p + 1) : "";

		cmd->execute(cin, cout, strCmd, args, ctx);
	}

	delete[] line;
}

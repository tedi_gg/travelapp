#pragma once

#include "CommandInterpreter.h"
#include "CIObject.h"
#include "../util/File.h"
#include "../util/VIterator.h"

using namespace std;

class Context
{
	CommandInterpreter *ci;
	File* openedFile;
	CIObject* object;
	bool fExit = false;

public:
	Context(CommandInterpreter*);

	bool isExit() { return fExit; }
	void exit() { fExit = true; }

	void doOpen(File& file, CIObject* object) {
		openedFile = new File(file);
		this->object = object;
	}

	void doClose();

	bool save(ostream& out);

	bool isSupportExtentsCmdList() {
		if (object != nullptr) {
			return object->isSupportExtentsCmdList();
		}

		return false;
	}

//	bool saveAs(File& f, ostream& out) {
//		if (openedFile != nullptr) {
//			return object->save(f, out);
//		}
//	}
//	
//	
//
	bool canOpenFile() { return (&(ci->getOpenHandler()) != nullptr); }
	bool isOpened() { return openedFile != nullptr;  }

	CIObject* getObject() const  { return object; }
	File& getOpenFile() const  { return *openedFile;  }
	OpenHandler* getOpenHandler() const  { return & (ci->getOpenHandler()); }
	
	void visitCommands(void (*visitor)(Command*, void*), void* arg);


};


#include "ExitCommand.h"
#include "Context.h"

void ExitCommand::help(std::ostream& out)
{
	out << "exit" << "\t\t\t\t" << "exits the program" << endl;
}

void ExitCommand::execute(std::istream&in, std::ostream&out, std::string&cmd, std::string&args, Context&ctx)
{
	if (args == "help") {
		help(out);
		return;
	}
	if (ctx.isOpened()) {
		if (ctx.getObject()->isModified()) {
			out << "There is opened file with changes do you want to save them?(y/n)" << endl;
			char c[2];
			in.getline(c,2);
			if (c[0] == 'y' || c[0] == 'Y') {
				if (ctx.save(out)) {
					out << "File saved" << endl;
				}
				else {
					out << "File cannot be saved" << endl;
				}
			}
		}
		ctx.doClose();
	}

	out << "Exiting the program..." << endl;
	ctx.exit();
}

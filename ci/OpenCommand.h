#pragma once

#include "Command.h"
#include <string>
using namespace std;

class OpenCommand :
	public Command
{
public:
	OpenCommand() : Command("open") {}
//	OpenCommand(const OpenCommand& cmd) : Command(cmd) {}
//	~OpenCommand();

	 void help(std::ostream&);
	 void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);

};


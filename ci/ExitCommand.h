#pragma once

#include "Command.h"

using namespace std;

class ExitCommand :
	public Command
{
public:
	ExitCommand() : Command("exit") {}
//	ExitCommand(const ExitCommand& cmd) : Command(cmd) {}
//	~ExitCommand() {};
//
	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};


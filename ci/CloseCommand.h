#pragma once

#include "Command.h"

class CloseCommand :
	public Command
{
public:
	CloseCommand() : Command("close") {}
//	CloseCommand(const CloseCommand& cmd) : Command(cmd) {}
//	~CloseCommand() {};


	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&) ;
};


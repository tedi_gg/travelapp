#include <vector>

#include "Context.h"


Context::Context(CommandInterpreter* ci)
{
	this->ci = ci;
	openedFile = nullptr;
	object = nullptr;
}

void Context::doClose()
{
		if (openedFile != nullptr) {
			delete openedFile;
			openedFile = nullptr;
		}

		if (object != nullptr) {
			delete object;
			object = nullptr;
		}
}

bool Context::save(ostream& out)
{
		if (openedFile != nullptr) {
			return object->save(*openedFile, out);
		}
}

void Context::visitCommands(void (*visitor)(Command*, void*), void* arg)
{
	if (isSupportExtentsCmdList()) {
		if (object->isNeedToBuildCmdList()) {
			object->build(new VIterator<Command*>(ci->orderedCommands));
		}
		object->visitCommands(visitor, arg);
	}
	else {
		for (vector<Command*>::iterator it = ci->orderedCommands->begin(); it < ci->orderedCommands->end(); it++) {
			(*visitor)(*it, arg);
		}
	}
}

//bool Context::open(File& file, std::ostream& out)
//{
//	OpenHandler handler = ci.getOpenHandler();
//
//	return false;
//}

#include "SaveCommand.h"
#include"Context.h"
#include "../util/Util.h"
void SaveCommand::help(std::ostream& os)
{
	os << "save" << "\t\t\t\t" << "saves the currently open file."<<endl;
}

void SaveCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string&args, Context&ctx)
{

	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}
	if (args.size() > 0) {
		out << "Incorrect arguments" << endl;
		help(out);
		return;
	}

	if (!ctx.canOpenFile()) {
		out << "The operation cannot be performed. Missing Open Handler." << endl;
		return;
	}

	if (ctx.isOpened()) {
		File file = ctx.getOpenFile();
		bool isSaved = true;
		if (ctx.getObject()->isModified()) {
			isSaved = ctx.save(out);
		}
		if (isSaved) {
			out << "Sucessfully saved " << file.getFileName() << endl;
		}
		else {
			out << "File cannot be saved" << endl;
		}
	}
	else {
		out << "There is no openned file.";
	}


}

#pragma once

#include <string>

//#include<fstream>
#include <iostream>

//#include "Context.h"


using namespace std;

class Context;

class Command
{
private:
	string name;
//
//	void cpy(const Command&);
//	void rmv();

protected:
	static bool numberCharValidator(char ch, ostream& out) {
		if (ch >= '0' && ch <= '9') {
			return true;
		}

		out << "Column number must be a number" << endl;
		return false;
	}

public:
	Command(const string& name);


	const string& getName() { return name; }
	virtual void help(ostream& ) = 0;
	virtual void execute(istream&, ostream&, string&, string&, Context&)  = 0;

};


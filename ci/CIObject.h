#pragma once

#include <iostream>
#include <fstream>

#include "../util/File.h"
#include "../util/VIterator.h"
#include "Command.h"

using namespace std;

class CIObject
{
public:
	virtual ~CIObject() {};

	virtual bool save(File& , std::ostream& ) = 0;
	virtual bool isModified() = 0;

	virtual bool isSupportExtentsCmdList() = 0;
	virtual bool isNeedToBuildCmdList() = 0;
	virtual void build(VIterator<Command*>* ciCommands) = 0;
	virtual Command* getCommand(string strCmd) = 0;
	virtual void visitCommands(void (*visitor)(Command*, void*), void* arg) = 0;


};


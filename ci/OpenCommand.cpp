#include <fstream>

#include "OpenCommand.h"
#include "Context.h"
#include "../util/Util.h"

void OpenCommand::help(std::ostream& out) 
{
	out << "open <file_path>" << "\t\t" << "Opens file <file_path>" << endl;
}

void OpenCommand::execute(std::istream& in, std::ostream& out, std::string&cmd, std::string& args, Context& ctx)
{
	if (args == "help") {
		help(out);
		return;
	}
	if (!ctx.canOpenFile()) {
		out << "The operation cannot be performed. Missing Open Handler." << endl;
		return;
	}

	File* file;

	trim(&args);

	if (args.size() == 0) {
		out << "Missing file argument" << endl;
		help(out);
		return;
	}

	if (ctx.isOpened()) {
		if (ctx.getObject()->isModified()) {
			//out << "There is opened file " << endl;
			// TODO invoke save or discard changes.
			out << "There is opened file with changes do you want to save them?(y/n)" << endl;
			char c[2];
			in.getline(c, 2);
			if (c[0] == 'y' || c[0] == 'Y') {
				if (ctx.save(out)) {
					out << "File saved" << endl;
				}
				else {
					out << "File cannot be saved" << endl;
				}
			}
		}
		ctx.doClose();
	}

	file = new File(args);

	OpenHandler* handler = ctx.getOpenHandler();

	CIObject* obj;
	if (!file->isExists()) {
		// Create new file and create new common object
		std::ofstream* fout = file->getOutputStream();
		fout->close();
		obj = handler->createEmptyObject(*file);
	}
	else {
		obj = handler->open(*file);
	}

	std::size_t found = args.find_last_of("/\\");

	if (obj != nullptr) {
		ctx.doOpen(*file, obj);
		out << "Sucessfully opened " << args.substr(found + 1) << "\n";
	}
	else {
		out << "File" << args.substr(found + 1) << " not opened \n";
	}

	delete file;
}

//OpenCommand::~OpenCommand() 
//{
//}

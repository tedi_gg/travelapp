#pragma once

#include <vector>
#include <map>
#include <string>

#include "Command.h"
#include "OpenHandler.h"

using namespace std;

class Context;

class CommandInterpreter
{
private:
	map<string, Command*> * commands;
	vector<Command*>* orderedCommands;

	OpenHandler* oHandler;

	void remove();
	void copy(const CommandInterpreter&);
	void addCommandImpl(const std::string&, Command*);

	friend Context;

public:
	CommandInterpreter(OpenHandler* );
	CommandInterpreter(const CommandInterpreter&);
	CommandInterpreter& operator=(const CommandInterpreter&);
	~CommandInterpreter();


	bool addCommand(Command*);
	void mainLoop(File* file = NULL, OpenHandler* oHandler = NULL);
	

	OpenHandler& getOpenHandler() { return *oHandler;  }
};


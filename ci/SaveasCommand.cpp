#include "SaveasCommand.h"
#include "Context.h"
#include "../util/Util.h"

void SaveasCommand::help(std::ostream& out)
{
	out << "saveas <file_name>" << "\t\t" << "saves currently open file in <file_name>" << endl;
}

void SaveasCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string&args, Context&ctx)
{
	if (args == "help") {
		help(out);
		return;
	}
	if (!ctx.canOpenFile()) {
		out << "The operation cannot be performed. Missing Open Handler." << endl;
		return;
	}

	trim(&args);

	if (args.size() == 0) {
		out << "Missing file argument" << endl;
		help(out);
		return;
	}

	File* file =new File(args);

	CIObject* obj = ctx.getObject();
	if (obj == nullptr) { 
		out << "There is no opened file." << endl;
		return;
	}

	std::size_t found = args.find_last_of("/\\");

	if (obj->save(*file, out)) {
		out << "Sucessfully closed " << args.substr(found + 1) << endl;
	}
	else {
		out << "File cannot be saved as " << args.substr(found + 1) << endl;
	}
}

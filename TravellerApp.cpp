// TravellerApp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "ci/CommandInterpreter.h"
#include "travel/ci/TravelOpenHandler.h"
#include "travel/ci/RegistrationCommand.h"
#include "travel/ci/LoginCommand.h"
#include "travel/ci/LogoutCommand.h"
#include "travel/ci/VisitsCommand.h"
#include "travel/ci/CommitCommand.h"
#include "travel/ci/AddVisitCommand.h"
#include "travel/ci/AddFriendCommand.h"
#include "travel/ci/RemoveFriendCommand.h"
#include "travel/ci/ListDestinationCommand.h"
#include "travel/ci/SearchFriendDestinationsCommand.h"
#include "travel/ci/DestinationInfoCommand.h"

using namespace std;

int main()
{
    std::cout << "Travel Application!" << endl;

    CommandInterpreter commandInterpreter(nullptr);
    commandInterpreter.addCommand(new RegistrationCommand());
    commandInterpreter.addCommand(new LoginCommand());
    commandInterpreter.addCommand(new CommitCommand());

  
    File* file = new File("users.db");
    vector<Command*> userCommands;
    userCommands.push_back(new LogoutCommand());
    userCommands.push_back(new VisitsCommand());
    userCommands.push_back(new AddVisitCommand());
    userCommands.push_back(new AddFriendCommand());
    userCommands.push_back(new RemoveFriendCommand());
    userCommands.push_back(new ListDestinationsCommand());
    userCommands.push_back(new SearchFriendDestinationsFriendCommand());
    userCommands.push_back(new DestinationInfoCommand());

    OpenHandler* openHandler = new TravelOpenHandler(userCommands);
    commandInterpreter.mainLoop(file, openHandler);
    delete openHandler;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

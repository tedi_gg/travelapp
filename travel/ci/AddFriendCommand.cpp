#pragma warning(disable:4996) 

#include "AddFriendCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"
#include "../../db/ci/CIUtil.h"
#include "../../db/dbms/StringDatabaseObject.h"

void AddFriendCommand::help(std::ostream& out)
{
	out << "addfriend <friend name>" << "\t\t" << "adding friend with name <friend name>" << endl;
}

void AddFriendCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}

	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <FriendName>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <description> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string friendName = targs;


	TravelObject* travel = (TravelObject*)ctx.getObject();

	Database* db = travel->getDatabase();

	User* user = travel->getUser();

	Table* t = db->getTable("users");

	TableStructure* ts = t->getTableStructure();

	Row* row = nullptr;
	VIterator<Row*>* it = t->iterateRows();
	bool isExistFriendUser = false;
	Column* column = ts->getColumn(0);
	string commaFriendName = '"' + friendName + '"';
	while (it->hasNext()) {
		row = it->next();
		DatabaseObject* dbo = row->getColumn(0);
		DatabaseObject* columnValue = convertStrToDbmsObj(column, out, commaFriendName);
		if (*dbo == *columnValue) {
			isExistFriendUser = true;
			break;
		}
	}

	if (!isExistFriendUser) {
		out << "Your friend doesn't exist in databaase" << endl;
		return;
	}
	string username = '"' + user->getUserName() + '"';
	delete it;
	it = t->iterateRows();

	while (it->hasNext()) {
		row = it->next();
		DatabaseObject* dbo = row->getColumn(0);
		DatabaseObject* columnValue = convertStrToDbmsObj(column, out, username);
		if (*dbo == *columnValue) {
			break;
		}
	}

	int friendsColumnIndex = ts->getColumnIndex("Friends");
	if (friendsColumnIndex == -1) {
		out << "Missing Friends columns" << endl;
		return;
	}

	DatabaseObject* dbObject = row->getColumn(friendsColumnIndex);

	string friendsList;
	bool emptyFirendsList = dbObject == nullptr || dbObject->getType() == ColumnType::NIL;
	if (emptyFirendsList) {
		friendsList = "";
	}
	else {
		friendsList = ((StringDatabaseObject*) dbObject)->getData();
		string subFriend;
		int beforeComma = 0;
		for (int i = 0; i < friendsList.length(); i++) {
			if (friendsList.at(i) == ',') {
				subFriend = friendsList.substr(beforeComma, i);
				if (subFriend == friendName) {
					out << "You already friend with this person." << endl;
					return;
				}
				beforeComma = i + 1;
			}
		}

		subFriend = friendsList.substr(beforeComma, friendsList.length());

		if (subFriend == friendName) {
			out << "You already friend with this person." << endl;
			return;
		}
	}

	if (emptyFirendsList) {
		friendsList = friendName;
	}
	else {
		friendsList.append("," + friendName);
	}

	Column* columnTarget = ts->getColumn(friendsColumnIndex);
	DatabaseObject* columnValue = new StringDatabaseObject(friendsList);
	if (row->size() > friendsColumnIndex) {
		row->setColumn(friendsColumnIndex, columnValue->clone());
	}
	else {
		while (row->size() < friendsColumnIndex) {
			row->addColumn(NilDatabaseObject::getInstance());
		}
		row->addColumn(columnValue->clone());
	}
	
	deleteDbmsObj(columnValue);

	File openFile = ctx.getOpenFile();
	string path = filePaths(openFile, "users.db");
	File* file = new File(path);
	ofstream* fout = file->getOutputStream();
	t->save(*fout);
	fout->close();
	delete fout;

}

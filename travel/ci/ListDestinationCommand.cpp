

#include "ListDestinationCommand.h"


#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"
#include "../../db/dbms/StringDatabaseObject.h"

void ListDestinationsCommand::help(std::ostream& out)
{
	out << "listdestinations" << "\t\t" << "returns all destinations in db" << endl;
}

void ListDestinationsCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}
	if (args.size() > 0) {
		out << "Incorrect arguments" << endl;
		help(out);
		return;
	}

	TravelObject* travel = (TravelObject*)ctx.getObject();
	Database* db = travel->getDatabase();
	
	if (travel->isDestinationsNeedToInit()) {
		travel->createDestinations();
		VIterator<Table*>* it = db->iterateTables();
		while (it->hasNext()) {
			Table* table = it->next();
			if (table->getName() != "users") {
				VIterator<Row*>* rows = table->iterateRows();
				while (rows->hasNext()) {
					Row* row = rows->next();
					DatabaseObject* value = row->getColumn(0);	// Destination
					if (value != nullptr && value->getType() == ColumnType::STRING) {
						const string sval = ((StringDatabaseObject*)value)->getData();
						travel->addDestination(sval);
					}
				}
				delete rows;
			}
		}

		delete it;
	}

	vector<string> destinations = travel->getDestinations();
	if (destinations.empty()) {
		out << "There is no destinations" << endl;
	}
	else {
		for (vector<string>::iterator it = destinations.begin(); it != destinations.end(); it++) {
			out << *it << endl;
		}
	}
}

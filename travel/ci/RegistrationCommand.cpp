
#include "RegistrationCommand.h"
#include "../../ci/Context.h"
#include "../../db/ci/DatabaseCIObject.h"
#include "../../util/Util.h"
#include "../tms/UserHelper.h"
#include "../../db/dbms/StringDatabaseObject.h"
#include "../../db/dbms/NilDatabaseObject.h"

using namespace std;

void RegistrationCommand::help(ostream& out)
{
	out << "registration <user_name> <password> <email>" << endl;
}

void RegistrationCommand::execute(istream& in, ostream& out, string& cmd, string& args, Context& ctx)
{
	DatabaseCIObject* dbo = (DatabaseCIObject*) ctx.getObject();
	if (dbo == nullptr) {
		out << "The context is not openned" << endl;
		return;
	}

	// Find user name
	trim(&args);
	int pos = args.find_first_of(" \n\r\t");
	if (pos == string::npos) {
		out << "missing next argument <password> and <email>" << endl;
		return;
	}
	string userName = args.substr(0, pos);

	int begin = args.find_first_not_of(" \n\r\t", pos);
	if (begin == string::npos) {
		out << "missing next argument <password> and <email>" << endl;
		return;
	}
	pos = args.find_first_of(" \n\r\t", begin);
	if (pos == string::npos) {
		out << "missing next argument <email>" << endl;
		return;
	}
	string password = args.substr(begin, pos - begin);

	begin = args.find_first_not_of(" \n\r\t", pos);
	if (begin == string::npos) {
		out << "missing next argument <email>" << endl;
		return;
	}
	string email = args.substr(begin);

	Database* db = dbo->getDatabase();
	if (db->getTable(userName) != nullptr) {
		out << "The user with name " << userName << " already exists" << endl;
		return;
	}

	Table* users = db->getTable("users");
	if (users == nullptr) {
		out << "Missing users table" << endl;
		return;
	}

	File openFile = ctx.getOpenFile();
	Table* t = createUserTable(openFile, userName,true);
	if (t != nullptr) {
		Row* row = new Row();
		row->addColumn(new StringDatabaseObject(userName));
		row->addColumn(new StringDatabaseObject(password));
		row->addColumn(new StringDatabaseObject(email));
		row->addColumn(NilDatabaseObject::getInstance());
		users->addRow(row);

		string path = filePaths(openFile, "users.db");
		File* file = new File(path);
		ofstream* fout = file->getOutputStream();
		users->save(*fout);
		fout->close();
		delete fout;
		delete file;

		db->addTable(t);
		out << "Created user " << userName << endl;
	}
	else {
		out << "Cannot create user " << userName << endl;
	}
}

#include "CommitCommand.h"
#include "../../ci/Context.h"
#include "../../util/Util.h"

void CommitCommand::help(std::ostream& out)
{
	out << "commit" << "\t\t" << "updates db" << endl;
}

void CommitCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}
	if (args.size() > 0) {
		out << "Incorrect arguments" << endl;
		help(out);
		return;
	}

		bool isSaved = true;
		isSaved = ctx.save(out);
		if (isSaved) {
			out << "Sucessfully saved "  << endl;
		}
		else {
			out << "DB cannot be saved" << endl;
		}
	

}

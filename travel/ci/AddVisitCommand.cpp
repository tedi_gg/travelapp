#pragma warning(disable:4996) 

#include "AddVisitCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"
#include "../../db/ci/CIUtil.h"
#include "../../db/dbms/StringDatabaseObject.h"


void AddVisitCommand::help(std::ostream& out)
{
	out << "addvisit <destination> <from> <to> " << "\t\t" << "Add new destination to our destination list" << endl
		<< "<grade> <comment> <photos>" << endl;

}

void AddVisitCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}
	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <<Description>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <description> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}

	string desttination = targs;


	if (desttination == "") {
		out << "Missing <description> argument" << endl;
		return;
	}
	// #2 <from>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <from> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}

	string from = targs;

	if (from == "") {
		out << "Missing <from> argument" << endl;
		return;
	}

	// #3 <to>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <to> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	string to = targs;

	if (to == "") {
		out << "Missing <to> argument" << endl;
		return;
	}

	// #4 <grade>
	targs += p;
	targs = skipws(targs);
	p = parseArg(targs, out, Command::numberCharValidator);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	int grade = atoi(targs);

	if (grade < 1 && grade > 5) {
		out << "Wrong grade (grade must be from 1-5)" << endl;
		return;
	}

	//#5 <comment>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <comment> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}

	string comment = targs;

	if (comment == "") {
		out << "Missing <comment> argument" << endl;
		return;
	}

	//#6 <photos>
	targs += p;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <comment> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}

	string photos = targs;

	targs += p;
	targs = skipws(targs);
	if (*targs != 0) {
		out << "There are extra symbols" << endl;
		delete[] arrayArgs;
		return;
	}

	delete[] arrayArgs;

	if (photos == "") {
		out << "Missing <photos> argument" << endl;
		return;
	}

	string sPhotos = photos;
	if (photos[0] == '"') {
		sPhotos = photos.substr(1, photos.length() - 2);
	}

	for (int i = 0; i < sPhotos.length(); i++) {
		if (sPhotos.at(i) == ',') {
			bool sufOk = false;
			if (i >= 4) {
				string suffix = ".png";
				sufOk = true;
				for (int j = 0; sufOk && j < 4; j++) {
					sufOk = sPhotos[j - 4 + i] == suffix[j];
				}
			}
			if (!sufOk && i >= 5) {
				string suffix = ".jpeg";
				sufOk = true;
				for (int j = 0; sufOk && j < 5; j++) {
					sufOk = sPhotos[j - 5 + i] == suffix[j];
				}
			}

			if (!sufOk) {
				out << "Wrong photo extentions" << endl;
				return;
			}
		}
	}
	//photos check
	{
		int i = sPhotos.length();
		bool sufOk = false;
		if (i >= 4) {
			string suffix = ".png";
			sufOk = true;
			for (int j = 0; sufOk && j < 4; j++) {
				sufOk = sPhotos[j - 4 + i] == suffix[j];
			}
		}
		if (!sufOk && i >= 5) {
			string suffix = ".jpeg";
			sufOk = true;
			for (int j = 0; sufOk && j < 5; j++) {
				sufOk = sPhotos[j - 5 + i] == suffix[j];
			}
		}

		if (!sufOk) {
			out << "Wrong photo extentions" << endl;
			return;
		}
	}
	//from check
	int fromYear, fromMonth, fromDate;
	{
		int i = from.length();
		string year, month, date;
		int before = 0,count = 0;
		if (i >= 9) {
			for (int j = 0; j < i; j++) {
				if (from[j] == '-') {
					switch (count) {
					case 0:
						year = from.substr(before + 1, j - 1 - before);
						if (j-1 - before != 4) {
							out << "Year must be yyyy-" << endl;
							return;
						}
						break;
					case 1:
						month = from.substr(before + 1, j - 1 - before);
						if (j - 1 - before != 2) {
							out << "Month must be -mm-" << endl;
							return;
						}
						break;
					default:out << "Error" << endl; break;
					}

					count++;
					before = j;

				}
				else if (count == 2 && j == i - 1) {
					date = from.substr(before + 1, j - 1 - before);
					if (j - 1 - before != 2) {
						out << "Date must be -dd" << endl;
						return;
					}
					break;
				}
			}
		}
		else {
			out << "Wrong year and month and date format is : yyyy-mm-dd " << endl;
			return;
		}
		fromYear = stoi(year);
		fromMonth = stoi(month);
		fromDate = stoi(date);
	
		if (fromMonth < 1 || fromMonth > 12) {
			out << "Wrong month inserted" << endl;
			return;
		}
		else if (fromDate >= 1 && fromDate <= 31) {
			switch (fromMonth) {
			case 01 :
			
				break;
			case 02:
				if ((fromYear % 400 == 0 ||
					(fromYear % 4 == 0 && fromYear % 100 != 0) && (fromDate > 29))) {
					out << "Wrong month and date in 02 month in " << fromYear << " days are 29" << endl;
					return;
				}
				else if (fromDate > 28) {
					out << "Wrong month and date in 02 month in " << fromYear << " days are 28"<< endl;
					return;
				}
				break;
			case 03:
				break;
			case 04:
				if (fromDate > 30) {
					out << "Wrong month and date in "<<fromMonth<<" month in " << fromYear << " days are 30" << endl;
					return;
				}
				break;
			case 05:
				break;
			case 06:
				if (fromDate > 30) {
					out << "Wrong month and date in " << fromMonth << " month in " << fromYear << " days are 30" << endl;
					return;
				}
				break;
			case 07:
				
			case 8:
				break;
			case 9:
				if (fromDate > 30) {
					out << "Wrong month and date in " << fromMonth << " month in " << fromYear << " days are 30" << endl;
					return;
				}
				break;
			case 10:
				break;
			case 11:	
				if (fromDate > 30) {
					out << "Wrong month and date in " << fromMonth << " month in " << fromYear << " days are 30" << endl;
				return;
				}
			   break;
			case 12:
				break;
			default: break;

			}
		}

	}
	//to check
	int toYear, toMonth, toDate;
	{
		int i = to.length();
		string year, month, date;
		int before = 0, count = 0;
		if (i >= 9) {
			for (int j = 0; j < i; j++) {
				if (to[j] == '-') {
					switch (count) {
					case 0:
						year = to.substr(before + 1, j - 1 - before);
						if (j - 1 - before != 4) {
							out << "Year must be yyyy-" << endl;
							return;
						}
						break;
					case 1:
						month = to.substr(before + 1, j - 1 - before);
						if (j - 1 - before != 2) {
							out << "Month must be -mm-" << endl;
							return;
						}
						break;
					default:out << "Error" << endl; break;
					}

					count++;
					before = j;

				}
				else if (count == 2 && j == i - 1) {
					date = to.substr(before + 1, j - 1 - before);
					if (j - 1 - before != 2) {
						out << "Date must be -dd" << endl;
						return;
					}
					break;
				}
			}
		}
		else {
			out << "Wrong year and month and date format is : yyyy-mm-dd " << endl;
			return;
		}
		toYear = stoi(year);
		toMonth = stoi(month);
		toDate = stoi(date);

		if (toMonth < 1 || toMonth > 12) {
			out << "Wrong month inserted" << endl;
			return;
		}
		else if (toDate >= 1 && toDate <= 31) {
			switch (toMonth) {
			case 01:

				break;
			case 02:
				if ((toYear % 400 == 0 ||
					(toYear % 4 == 0 && toYear % 100 != 0) && (toDate > 29))) {
					out << "Wrong month and date" << endl;
					return;
				}
				else if (toDate > 28) {
					out << "Wrong month and date" << endl;
					return;
				}
				break;
			case 03:
				break;
			case 04:
				if (toDate > 30) {
					out << "Wrong month and date" << endl;
					return;
				}
				break;
			case 05:
				break;
			case 06:
				if (toDate > 30) {
					out << "Wrong month and date" << endl;
					return;
				}
				break;
			case 07:

			case 8:
				break;
			case 9:
				if (toDate > 30) {
					out << "Wrong month and date" << endl;
					return;
				}
				break;
			case 10:
				break;
			case 11:
				if (toDate > 30) {
					out << "Wrong month and date" << endl;
					return;
				}
				break;
			case 12:
				break;
			default: break;
			}
		}
		if (toYear < fromYear) {
			out << "Can't to year be less than from year" << endl;
			return;
		}
		else if (toYear == fromYear) {
			if (toMonth < fromMonth) {
				out << "Can't to month be less than from month" << endl;
				return;
			}
			else if (toMonth == fromMonth) {
				if (toDate < fromDate) {
					out << "Can't to date be less than from date" << endl;
					return;
				}
			}
		}

	}

	//adding visit 

	vector<string> columns;
	columns.push_back(desttination);
	columns.push_back(from);
	columns.push_back(to);
	columns.push_back(to_string(grade));
	columns.push_back(comment);
	columns.push_back(photos);





	TravelObject* travel = (TravelObject*)ctx.getObject();

	Database* db = travel->getDatabase();

	User* user = travel->getUser();

	Table* t = db->getTable(user->getUserName());

	TableStructure* ts = t->getTableStructure();
	Row* row = new Row();
	int index = 0;
	for (; index < ts->getColumnSize(); index++) {
		Column* column = ts->getColumn(index);
		DatabaseObject* value = convertStrToDbmsObj(column, out, columns[index]);
		if (value == nullptr) {
			delete row;
			deleteDbmsObj(value);
			return;
		}
		row->addColumn(value);
	}
	t->addRow(row);

	DatabaseObject* value = row->getColumn(0);
	if (value != nullptr && value->getType() == ColumnType::STRING) {
		string sval = ((StringDatabaseObject*)value)->getData();
		travel->addDestination(sval);
	}


}

#pragma once

#include "../../ci/Command.h"

using namespace std;

class RemoveFriendCommand : public Command {
public:
	RemoveFriendCommand() : Command("removefriend") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};


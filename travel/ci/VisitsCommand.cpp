#include "VisitsCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"

void VisitsCommand::help(std::ostream& out)
{
	out << "visits" << "\t\t" << "Shows all your visited destinations." << endl;
}

void VisitsCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}

	TravelObject* travel = (TravelObject*)ctx.getObject();
	
	Database* db = travel->getDatabase();
	
	User* user = travel->getUser();
	
	Table*t = db->getTable(user->getUserName());

	TableStructure* ts = t->getTableStructure();
	VIterator<Column*>* it = ts->iterator();
	Column* column = nullptr;

	int count = -1;
	while (it->hasNext()) {
	    column = it->next();
		if (column->getName() == "Destination") {
			count++;
			break;
		}
	}
	delete it;
	if (count == -1) {
		out << "Missing Destination" << endl;
		return;
	}

	VIterator<Row*>* it1 = t->iterateRows();
	out << "Your destinations: " << endl;
	while (it1->hasNext()) {
		Row* row = it1->next();
		DatabaseObject* obj1 = row->getColumn(count);
		if (obj1 != nullptr) {
			out << obj1->toString() << endl;
		}
	}


	delete it1;



}

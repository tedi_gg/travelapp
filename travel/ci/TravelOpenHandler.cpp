
#include "TravelOpenHandler.h"
#include "../../util/Util.h"
#include "../../util/VIterator.h"
#include "../../db/dbms/StringDatabaseObject.h"
#include "../tms/UserHelper.h"
#include "TravelObject.h"

using namespace std;

Table* TravelOpenHandler::loadUserTable(File& file, string& userName)
{
	return createUserTable(file, userName,false);
}

CIObject* TravelOpenHandler::open(File& file)
{
	return createEmptyObject(file);
}

CIObject* TravelOpenHandler::createEmptyObject(File& file)
{
	Database* db = new Database();
	
	string usersDbFileName("users.db");

	string filePath = filePaths(file, usersDbFileName);
	File* fUsersDB = new File(filePath); // delete in Table
	Table* usersTable = new Table("users", fUsersDB, usersDbFileName);
	if (fUsersDB->isExists()) {
		usersTable->open();
	}
	else {
		// Create structure.
		// SUserName,SPassword,SEmail
		usersTable->addColumn(new Column("UserName", ColumnType::STRING));
		usersTable->addColumn(new Column("Password", ColumnType::STRING));
		usersTable->addColumn(new Column("Email", ColumnType::STRING));
		usersTable->addColumn(new Column("Friends", ColumnType::STRING));
		ofstream* out = fUsersDB->getOutputStream();
		usersTable->save(*out);
		out->close();
		delete out;
	}
	db->addTable(usersTable);

	VIterator<Row*>* it = usersTable->iterateRows();
	while(it->hasNext()) {
		Row* row = it->next();

		DatabaseObject* obj = row->getColumn(0);
		if (obj != nullptr && obj->getType() == ColumnType::STRING) {
			StringDatabaseObject* strDbObj = (StringDatabaseObject *) obj;
			string userName = strDbObj->getData();
			if (db->getTable(userName) != nullptr) {
				cout << "Duplicate user " << userName << endl;
				continue;
			}

			Table* t = loadUserTable(*fUsersDB, userName);
			if (t != nullptr) {
				db->addTable(t);
			}
		}
	}

	delete it;

	TravelObject* travelObject = new TravelObject(db);
	for (vector<Command*>::iterator it = commands.begin(); it != commands.end(); it++) {
		travelObject->addCommand(*it);
	}
	return travelObject;
}



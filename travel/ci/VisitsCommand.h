#pragma once

#include "../../ci/Command.h"

using namespace std;

class VisitsCommand : public Command {
public:
	VisitsCommand() : Command("visits") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};
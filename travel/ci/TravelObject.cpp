#include "TravelObject.h"

TravelObject::~TravelObject()
{
	if (tempCommands != nullptr) {
		delete tempCommands;
	}
	delete userCommands;
	if (destinations != nullptr) {
		delete destinations;
	}
}

void TravelObject::setUser(User* user)
{
	if (this->user != nullptr) {
		delete this->user;
	}

	this->user = user;
}

void TravelObject::build(VIterator<Command*>* ciCommands)
{
	tempCommands = new vector<Command*>();
	while (ciCommands->hasNext()) {
		Command* cmd = ciCommands->next();
		if (cmd != nullptr && (cmd->getName() == "help" || cmd->getName() == "commit")) {
			tempCommands->push_back(cmd);
		}
	}
	delete ciCommands;

	vector<Command*>::iterator it = userCommands->begin();
	while (it != userCommands->end()) {
		tempCommands->push_back(*it);
		it++;
	}
}

Command* TravelObject::getCommand(string strCmd)
{
	vector<Command*>::iterator it = tempCommands->begin();
	while (it != tempCommands->end()) {
		if ((*it)->getName() == strCmd) {
			return *it;
		}
		it++;
	}

	return nullptr;
}

void TravelObject::visitCommands(void(*visitor)(Command*, void*), void* arg)
{
	for (vector<Command*>::iterator it = tempCommands->begin(); it < tempCommands->end(); it++) {
		(*visitor)(*it, arg);
	}

}

bool TravelObject::save(File& file, std::ostream& out)
{
	Database* database = getDatabase();
	for (VIterator<Table*>* it = database->iterateTables(); it->hasNext();) {
		Table* t = it->next();
		string filePath = t->getFileName();

		File* f = new File(filePath);
		t->save(*f->getOutputStream());
	}

	return true;
}

void TravelObject::addDestination(string destination)
{
	if (destinations != nullptr) {
		map<string,int>::iterator it = destinations->find(destination);
		if (it == destinations->end()) {
			destinations->emplace(destination, 1);
		}
		else {
			(*destinations)[destination] = (*destinations)[destination] + 1;
		}
	}
}

vector<string> TravelObject::getDestinations()
{
	vector<string> rval;
	for (map<string, int>::iterator it = destinations->begin(); it != destinations->end(); it++) {
		if (it->second != 0) {
			rval.push_back(it->first);
		}
	}

	return rval;
}


#pragma once

#include "../../db/ci/DatabaseOpenHandler.h"
#include "../../util/File.h"

class TravelOpenHandler : public DatabaseOpenHandler {
	vector<Command*> commands;
	Table* loadUserTable(File&, string& userName);

public:
	TravelOpenHandler(vector<Command*> aCommands) : commands(aCommands) {};
	~TravelOpenHandler() {
		vector<Command*>::iterator it = commands.begin();
		while (it != commands.end()) {
			delete* it;
		}
	}
	CIObject* open(File&);
	CIObject* createEmptyObject(File&);

};

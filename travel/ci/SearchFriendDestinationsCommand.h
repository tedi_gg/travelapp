#pragma once


#include "../../ci/Command.h"

using namespace std;

class SearchFriendDestinationsFriendCommand : public Command {
public:
	SearchFriendDestinationsFriendCommand() : Command("searchfrienddestinations") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};
#pragma once

#include "../../db/ci/DatabaseCIObject.h"
#include "../tms/User.h"

class TravelObject : public DatabaseCIObject {

	User* user;
	vector<Command*>* userCommands;
	vector<Command*>* tempCommands;

	map<string, int>* destinations;


public:
	TravelObject(Database* database) : DatabaseCIObject(database), userCommands(new vector<Command*>()), tempCommands(nullptr) {}
	~TravelObject();

	void addCommand(Command* cmd) { userCommands->push_back(cmd); }

	void setUser(User* user);

	User* getUser() { return user; }

	bool isSupportExtentsCmdList() { 
		return user != nullptr;
	}

	bool isNeedToBuildCmdList() { return tempCommands == nullptr; }
	void build(VIterator<Command*>* ciCommands);
	Command* getCommand(string strCmd);
	void visitCommands(void (*visitor)(Command*, void*), void* arg);

	bool save(File& file, std::ostream& out);

	bool isDestinationsNeedToInit() { return destinations == nullptr; }
	void createDestinations() { destinations = new map<string, int>(); }
	void addDestination(string destination);

	vector<string> getDestinations();

};

#pragma once

#include "../../ci/Command.h"

using namespace std;

class LoginCommand : public Command {
public:
	LoginCommand() : Command("login") {}
	//	RegistrationCommand(const RegistrationCommand& cmd) : Command(cmd) {}
	//	~RegistrationCommand() {};


	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};

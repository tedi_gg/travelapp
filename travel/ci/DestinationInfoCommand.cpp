#pragma warning(disable:4996) 

#include "DestinationInfoCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"
#include "../../db/ci/CIUtil.h"
#include "../../db/dbms/StringDatabaseObject.h"
#include "../../db/dbms/IntegerDatabaseObject.h"

void DestinationInfoCommand::help(std::ostream& out)
{
	out << "destinationinfo <destination_name>" << "\t\t" << "shows info about current destination with name <destination_name>" << endl;
}

void DestinationInfoCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}

	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <FriendName>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <description> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string destinationName = targs;

	TravelObject* travel = (TravelObject*)ctx.getObject();

	Database* db = travel->getDatabase();

	Table* t = db->getTable("users");
	Table* currentUserTable;

	TableStructure* ts = t->getTableStructure();
	TableStructure* currentUserTs;

	Row* row = nullptr;
	Row* curretUserRow = nullptr;
	VIterator<Row*>* it = t->iterateRows();


	//int destinationsColumnIndex = currentUserTs->getColumnIndex("Destination");
	DatabaseObject* dbObject = nullptr;
	string destinationsList;
	long gradeList;
	string currentUsername;
	vector<long> avarageGrade;
	int i = 0;

	while (it->hasNext()) {
		row = it->next();
		DatabaseObject* userObject = row->getColumn(0);
		currentUsername = ((StringDatabaseObject*)userObject)->getData();
		currentUserTable = db->getTable(currentUsername);
		currentUserTs = currentUserTable->getTableStructure();

		int destinationsColumnIndex = currentUserTs->getColumnIndex("Destination");
		int gradeColumnIndex = currentUserTs->getColumnIndex("Grade");

		dbObject = row->getColumn(destinationsColumnIndex);
		bool emptyDestinationsList = dbObject == nullptr || dbObject->getType() == ColumnType::NIL;
		if (emptyDestinationsList) {
			continue;
		}
		else {
			VIterator<Row*>* userIt = currentUserTable->iterateRows();
			while (userIt->hasNext()) {
				curretUserRow = userIt->next();
				DatabaseObject* destinationObj = curretUserRow->getColumn(0);

				destinationsList = ((StringDatabaseObject*)destinationObj)->getData();
				if (destinationsList == destinationName) {
					destinationObj = curretUserRow->getColumn(gradeColumnIndex);
					gradeList = ((IntegerDatabaseObject*)destinationObj)->getData();
					out << "Destination:" << destinationName << " visited by:" << currentUsername << " with grade:" << gradeList << endl;
					avarageGrade.push_back(gradeList);
					i++;
				}
			}
		}

	}

	int sum = 0;
	int j = 0;
	for (; j < i; j++) {
		sum += avarageGrade[j];
	}
	double avg = sum / j;
	out << "Avarage grade for destination with name:" << destinationName << " is " << avg << endl;


}

#include "LogoutCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"

void LogoutCommand::help(std::ostream& out)
{
	out << "Logout" << endl;
}

void LogoutCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{
	TravelObject *travel = (TravelObject*) ctx.getObject();
	if (travel != nullptr) {
		User* user = travel->getUser();
		if (user != nullptr) {
			out << "Logout of user " << user->getUserName() << endl;
			travel->setUser(nullptr);
			return;
		}
	}

	out << "Cannot logout" << endl;
}

#pragma once

#include "../../ci/Command.h"

using namespace std;

class LogoutCommand : public Command {
public:
	LogoutCommand() : Command("logout") {}
	//	RegistrationCommand(const RegistrationCommand& cmd) : Command(cmd) {}
	//	~RegistrationCommand() {};


	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};

#pragma once

#include "../../ci/Command.h"

using namespace std;

class AddVisitCommand : public Command {
public:
	AddVisitCommand() : Command("addvisit") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
}; 

#include "LoginCommand.h"
#include "../../ci/Context.h"
#include "../../util/Util.h"
#include "TravelObject.h"
#include "../../db/dbms/Database.h"
#include "../../util/VIterator.h"
#include "../../db/dbms/StringDatabaseObject.h"

using namespace std;

void LoginCommand::help(ostream& out)
{
	out << "login <user_name>\t\t login" << endl;
}

void LoginCommand::execute(istream& in, ostream& out, string& cmd, string& args, Context& ctx)
{

	TravelObject* dbo = (TravelObject*)ctx.getObject();
	if (dbo == nullptr) {
		out << "The context is not openned" << endl;
		return;
	}

	// Find user name
	trim(&args);

	string userName = args;

	User* user = dbo->getUser();
	if (user != nullptr) {
		string oldUserName = user->getUserName();
		if (oldUserName == userName) {
			out << "Already login with name " << userName << endl;
			return;
		}

		out << "You are login with user " << oldUserName << endl;
		out << "Whould you like do logout and the login with new user (Y/N)" << endl;
		char c = in.get();
		if (c != 'Y' && c != 'y') {
			out << "Login cancel" << endl;
			return;
		}

		dbo->setUser(nullptr);
	}

	Database* db = dbo->getDatabase();
	Table* table = db->getTable("users");
	if (table == nullptr) {
		out << "Missing users table" << endl;
		return;
	}

	Row* userRow = nullptr;

	VIterator<Row*>* it = table->iterateRows();
	while (userRow == nullptr && it->hasNext()) {
		Row* row = it->next();
		DatabaseObject* o =  row->getColumn(0);
		if (o != nullptr && o->getType() == ColumnType::STRING) {
			StringDatabaseObject* sdo = (StringDatabaseObject*)o;
			if (userName == sdo->getData()) {
				userRow = row;
			}
		}
	}

	if (userRow == nullptr) {
		out << "Cannot login (1) " << endl;
		return;
	}

	DatabaseObject* o = userRow->getColumn(1);
	if (o != nullptr && o->getType() == ColumnType::STRING) {
		StringDatabaseObject* sdo = (StringDatabaseObject*)o;
		out << "Enter Password:";
		char* buf = new char[4096];
		in.getline(buf, 4096);
		string s(buf);
		if (sdo->getData() != s) {
			out << "Cannot login (2) " << endl;
			return;
		}
	}

	user = new User(userName);

	dbo->setUser(user);

	out << "Login successful" << endl;

}

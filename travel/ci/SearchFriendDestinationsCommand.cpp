#pragma warning(disable:4996) 

#include "SearchFriendDestinationsCommand.h"

#include "../../ci/Context.h"
#include "TravelObject.h"
#include "../tms/User.h"
#include "../../util/Util.h"
#include "../../db/ci/CIUtil.h"
#include "../../db/dbms/StringDatabaseObject.h"

void SearchFriendDestinationsFriendCommand::help(std::ostream& out)
{
	out << "searchfrienddestinations <friend_name>" << "\t\t" << "showing all destinations and comments of your friend with name  <friend_name>" << endl;
}

void SearchFriendDestinationsFriendCommand::execute(std::istream& in, std::ostream& out, std::string& cmd, std::string& args, Context& ctx)
{

	trim(&args);
	if (args == "help") {
		help(out);
		return;
	}

	if (args == "") {
		// TODO
		out << "Missing arguments" << endl;
		help(out);
		return;
	}

	char* arrayArgs = new char[args.length() + 1];
	strcpy(arrayArgs, args.c_str());

	// #1 <FriendName>
	char* targs = arrayArgs;
	int p = 0;
	targs = skipws(targs);
	if (*targs == 0) {
		out << "Missing <description> argument" << endl;
		delete[] arrayArgs;
		return;
	}
	p = parseValueArg(targs, out);
	if (p == -1) {
		delete[] arrayArgs;
		return;
	}
	if (*targs == '"') {
		// close quote is p-1 or p-2 (depends on if the end of string is reached).
		if (targs[p - 1] == '"') {
			targs[p - 1] = 0;
		}
		else if (targs[p - 2] == '"') {
			targs[p - 2] = 0;
		}
		else {
			out << "Error in quotes";
		}
		targs++;
		p--;
	}

	string friendName = targs;

	TravelObject* travel = (TravelObject*)ctx.getObject();

	Database* db = travel->getDatabase();

	User* user = travel->getUser();

	Table* t = db->getTable("users");

	Table* userTable;

	TableStructure* ts = t->getTableStructure();

	Row* row = nullptr;
	VIterator<Row*>* it = t->iterateRows();
	bool isExistFriendUser = false;
	Column* column = ts->getColumn(0);
	string commaFriendName = '"' + friendName + '"';
	while (it->hasNext()) {
		row = it->next();
		DatabaseObject* dbo = row->getColumn(0);
		DatabaseObject* columnValue = convertStrToDbmsObj(column, out, commaFriendName);
		if (*dbo == *columnValue) {
			isExistFriendUser = true;
			break;
		}
	}

	if (!isExistFriendUser) {
		out << "Your friend doesn't exist in databaase" << endl;
		return;
	}

	


	userTable = db->getTable(friendName);
	TableStructure* userTableStructure = userTable->getTableStructure();
	
	delete it;
	it = userTable->iterateRows();


	int destinationsColumnIndex = userTableStructure->getColumnIndex("Destination");
	int commentColumnIndex = userTableStructure->getColumnIndex("Comment");
	if (destinationsColumnIndex == -1) {
		out << "Missing Destination columns" << endl;
		return;
	}

	DatabaseObject* dbObject;
	string destinationsList;
	string commentList;

	while (it->hasNext()) {
		row = it->next();
		dbObject = row->getColumn(destinationsColumnIndex);
		bool emptyDestinationsList = dbObject == nullptr || dbObject->getType() == ColumnType::NIL;
		if (emptyDestinationsList) {
			out << "You're friend hasn't got any destinations visited." << endl;
			return;
		}
		else {
			destinationsList = ((StringDatabaseObject*)dbObject)->getData();
			dbObject = row->getColumn(commentColumnIndex);
			commentList = ((StringDatabaseObject*)dbObject)->getData();
			out << destinationsList << "\t\t" <<commentList << endl;
		}

	}

}

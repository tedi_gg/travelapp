#pragma once


#include "../../ci/Command.h"

using namespace std;

class DestinationInfoCommand : public Command {
public:
	DestinationInfoCommand() : Command("destinationinfo") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};
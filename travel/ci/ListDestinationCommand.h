#pragma once


#include "../../ci/Command.h"

using namespace std;

class ListDestinationsCommand : public Command {
public:
	ListDestinationsCommand() : Command("listdestinations") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
}; 

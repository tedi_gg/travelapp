#pragma once

#include "../../ci/Command.h"

using namespace std;

class AddFriendCommand : public Command {
public:
	AddFriendCommand() : Command("addfriend") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};


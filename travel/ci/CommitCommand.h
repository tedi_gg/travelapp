#pragma once

#include "../../ci/Command.h"

using namespace std;

class CommitCommand : public Command {
public:
	CommitCommand() : Command("commit") {}

	void help(std::ostream&);
	void execute(std::istream&, std::ostream&, std::string&, std::string&, Context&);
};
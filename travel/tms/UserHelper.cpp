
#include "UserHelper.h"
#include "../../util/Util.h"


using namespace std;

Table* createUserTable(File& file, string& userName, bool isRegistration) {
	string filePath = filePaths(file, userName+".db");
	File* f = new File(filePath);
	Table* table = new Table(userName, f, userName+".db");
	if (f->isExists() && !isRegistration) {
		table->open();
	}
	else {
		// Create structure.
		// SDestination,SFrom,STo,IGrade,SComment,SPhotos
		table->addColumn(new Column("Destination", ColumnType::STRING));
		table->addColumn(new Column("From", ColumnType::STRING));
		table->addColumn(new Column("To", ColumnType::STRING));
		table->addColumn(new Column("Grade", ColumnType::INT));
		table->addColumn(new Column("Comment", ColumnType::STRING));
		table->addColumn(new Column("Photos", ColumnType::STRING));
		ofstream* out = f->getOutputStream();
		table->save(*out);
		out->close();
		delete out;
	}

	return table;

}
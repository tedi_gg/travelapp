#pragma once

#include <string>

using namespace std;

class User {
	string userName;
public:
	User(string& userName) : userName(userName) {}

	string& getUserName() { return userName; }
};

#pragma once

#include <string>

#include "../../util/File.h"
#include "../../db/dbms/Table.h"

using namespace std;

Table* createUserTable(File&, string&,bool);

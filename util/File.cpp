#include "File.h"

string File::getFileName() const 
{
	string temp = *fname;
	std::size_t found = temp.find_last_of("/\\");

	return temp.substr(found + 1);
}

string File::getFileFullName() const 
{
	return *fname;
}

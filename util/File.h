#pragma once

#include <string>
#include <fstream>

using namespace std;

class File
{
private:
	string* fname;

public:
	File(const char* name) { fname = new string(name); }
	File(string& name) {
		this->fname = new string(name);
	}

	bool isExists() {
		ifstream f(*fname);
		return f.good();
	}

	ofstream* getOutputStream() const  {
		ofstream* out = new ofstream(*fname);
		return out;
	}

	ifstream* getInputStream() const  {
		return new ifstream(*fname);
	 }

	string getFileName() const ;
	string getFileFullName() const ;

};


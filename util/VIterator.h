#pragma once

#include <vector>
#include <iostream>
using namespace std;


template <typename T>
class VIterator {
	vector<T>* vec;
	typename vector<T>::iterator begin;
	typename vector<T>::iterator cursor;
	typename vector<T>::iterator end;
	

public:
	VIterator(vector<T>* vec);
	VIterator(typename vector<T>::iterator begin, typename vector<T>::iterator end);

	void beforeFirst();
	void afterLast();

	bool hasPrevious();
	T previous();

	bool hasNext();
	T next();
	bool remove();
};


template<typename T>
inline VIterator<T>::VIterator(vector<T>* vec)
{
	this->vec = vec;
	this->cursor = vec->begin();
	this->begin = this->cursor;
	this->end = vec->end();
}

template<typename T>
inline VIterator<T>::VIterator(typename vector<T>::iterator begin, typename vector<T>::iterator end)
{
	this->vec = nullptr;
	this->begin = begin;
	this->cursor = begin;
	this->end = end;
}

template<typename T>
inline void VIterator<T>::beforeFirst()
{
	cursor = begin;
}

template<typename T>
inline void VIterator<T>::afterLast()
{
	cursor = end;
}

template<typename T>
inline bool VIterator<T>::hasPrevious()
{
	return &cursor != nullptr && cursor > begin;
}

template<typename T>
inline T VIterator<T>::previous()
{
	if (hasPrevious()) {
		return *(--cursor);
	}

	return nullptr;
}

template<typename T>
inline bool VIterator<T>::hasNext()
{
	return &cursor != nullptr && cursor < end;
}

template<typename T>
inline T VIterator<T>::next()
{
	if (hasNext()) {
		return *cursor++;
	}

	return nullptr;
}



template<typename T>
inline bool VIterator<T>::remove()
{
	if (vec != nullptr && &cursor != nullptr && cursor > vec->begin()) {
		if (cursor == end) {
			cursor--;
			vec->erase(cursor);
			end = vec->end();
			cursor = end;
		} 
		else {
			cursor--;
			if (cursor == begin) {
				vec->erase(cursor);
				begin = vec->begin();
			}
			else {
				vec->erase(cursor);
			}

			cursor++;
		}
	
		return true;
	}

	return false;

}
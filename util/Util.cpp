#include "Util.h"


int parseArg(char* args, ostream& out) {
	char* p = args;
	while (*p && *p != ' ') {
		p++;
	}

	// return the next afte space, but if it is 0 - then return this 
	if (*p) {
		*p = 0;
		p++;	
	}

	if (p == args) {
		out << "Missing argument" << endl;
		return -1;
	}

	return p - args;
}


int parseArg(char* args, ostream& out, bool (charValidator)(char, ostream&)) {
	char* p = args;
	while (*p && *p != ' ') {
		if (!charValidator(*p, out)) {
			return -1;
		}
		p++;
	}

	// return the next afte space, but if it is 0 - then return this 
	if (*p) {
		*p = 0;
		p++;
	}

	if (p == args) {
		out << "Missing argument" << endl;
		return -1;
	}

	return p - args;
}


int parseValueArg(char* args, ostream& out) {
	char* p = args;
	if (*p != '"') {
		return parseArg(args, out);
	}

	p++;
	while (*p && *p != '"') {
		if (*p == '\\') {
			p++;
			if (*p == 0) {
				continue;
			}
		}
		p++;
	}

	if (*p != '"') {
		out << "Missing closed quote in parameter" << endl;
		return -1;
	}

	p++;
	if (*p && *p != ' ') {
		out << "There is no whitespace after close quote char" << endl;
		return -1;
	}

	// return the next afte space, but if it is 0 - then return this 
	if (*p) {
		*p = 0;
		p++;
	}

	return p - args;
}

#include <iostream>

#include "../../ci/CIObject.h"
#include "DatabaseOpenHandler.h"
#include "../dbms/Database.h"
#include "../dbms/Table.h"
#include "../../util/VIterator.h"

CIObject* DatabaseOpenHandler::open(File& file)
{
	ifstream* in = file.getInputStream();
	Database* database = new Database();
	char* buffer = new char[4096];
	
	while (!in->eof()) {
		in->getline(buffer, 4096);
		int i, len = strlen(buffer);
		for (i = 0; i < len; i++) {
			if (buffer[i] == ',') {
				break;
			}
		}
		if (i < len) {
			buffer[i] = 0;
		}

		string name = buffer;
		string args = (i < len) ? buffer + (i + 1) : "";

		if (args == "") {
			cout << "Cannot create table " << name << " because it is empty." << endl;
			continue;
		}
		string file_path;

#ifdef _WIN32
		if (args[0] == '\\' || args.size() > 3 && args[0] >= 'A' && args[0] <= 'Z' && args[1] == ':' && args[2] == '\\')  {
			file_path = args;
		}
		else {
			string baseFileName = file.getFileFullName();
			std::size_t found = baseFileName.find_last_of("\\");
			if (found == string::npos) {
				file_path = args;
			}
			else {
				file_path = baseFileName.substr(0, found) + args;
			}
		}
#else
		if (args[0] == '/') {
			file_path = args;
		}
		else {
			string baseFileName = file.getFileFullName();
			std::size_t found = baseFileName.find_last_of("/");
			if (found == string::npos) {
				file_path = args;
			}
			else {
				file_path = baseFileName.substr(0, found) + args;
			}
		}
#endif
		
		File* f = new File(file_path);
		Table* table = new Table(name,f,file_path);
		database->addTable(table);

	}

	in->close();
	
	//Loading tables data
	for (VIterator<Table*>* it = database->iterateTables(); it->hasNext();) {
		Table* t = it->next();
		t->open();
	}
	
	return new DatabaseCIObject(database);
}

CIObject* DatabaseOpenHandler::createEmptyObject(File& f)
{
	return new DatabaseCIObject(new Database());
}

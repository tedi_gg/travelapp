#pragma once


#include <iostream>

#include "../dbms/DatabaseObject.h"
#include "../dbms/Column.h"
#include "../dbms/ColumnType.h"

using namespace std;

DatabaseObject* convertStrToDbmsObj(Column* column, ostream& out, string& value);
inline void deleteDbmsObj(DatabaseObject* obj) {
	if (obj != nullptr && obj->getType() != ColumnType::NIL) {
		delete obj;
	}
}

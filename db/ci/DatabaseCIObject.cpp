#include "DatabaseCIObject.h"


void DatabaseCIObject::copy(const DatabaseCIObject& other)
{
	database = other.database;
	modified = other.modified;
}

void DatabaseCIObject::remove()
{
	delete database;
}

DatabaseCIObject::DatabaseCIObject(const DatabaseCIObject& other)
{
	copy(other);
}

DatabaseCIObject& DatabaseCIObject::operator=(const DatabaseCIObject& other)
{
	if (this != &other) {
		remove();
		copy(other);
	}
	return *this;
}

DatabaseCIObject::~DatabaseCIObject()
{
	remove();
}

bool DatabaseCIObject::save(File& file, std::ostream& out)
{
	ofstream* of = file.getOutputStream();
	string dbName;
	string dbPath;
	for(VIterator<Table*>* it = database->iterateTables(); it->hasNext();){
		Table* t = it->next();
		dbName = t->getName();
		dbPath = t->getFileName();
		*of << dbName << "," << dbPath << endl;
		database->save(*t->getFile());
	}
	of->close();
	return false;
}

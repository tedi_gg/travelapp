#pragma once

#include "../../ci/CIObject.h"
#include "../dbms/Database.h"
class DatabaseCIObject : public CIObject {
private:
	Database* database;
	bool modified = false;
	
	void copy(const DatabaseCIObject&);
	void remove();
public:
	DatabaseCIObject(Database* database) : database(database) {}
	DatabaseCIObject(const DatabaseCIObject&);
	DatabaseCIObject& operator=(const DatabaseCIObject&);
	virtual ~DatabaseCIObject();

	bool save(File&, std::ostream&);
	bool isModified() { return modified; }

	Database* getDatabase() { return database; }

	void setModified() { modified = true; }

	virtual bool isSupportExtentsCmdList() { return false; }
	virtual bool isNeedToBuildCmdList() { return false; }
	virtual void build(VIterator<Command*>* ciCommands) {}
	virtual Command* getCommand(string strCmd) { return nullptr; }
	virtual void visitCommands(void (*visitor)(Command*, void*), void* arg) {} ;

};


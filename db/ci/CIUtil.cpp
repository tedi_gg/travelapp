#include "CIUtil.h"
#include "../dbms/ColumnType.h"
#include "../dbms/NilDatabaseObject.h"
#include "../dbms/IntegerDatabaseObject.h"
#include "../dbms/DoubleDatabaseObject.h"
#include "../dbms/StringDatabaseObject.h"
#include "../dbms/Column.h"

DatabaseObject* convertStrToDbmsObj(Column* column, ostream& out, string& value)
{
	if (value == "NULL") {
		return NilDatabaseObject::getInstance();
	}

	ColumnType type = column->getType();
	DatabaseObject* columnValue = nullptr;
	switch (type)
	{
	case ColumnType::NIL:
		break;
	case ColumnType::INT:
		columnValue = IntegerDatabaseObject::load(value);
		break;
	case ColumnType::DOUBLE:
		columnValue = DoubleDatabaseObject::load(value);
		break;
	case ColumnType::STRING:
		columnValue = StringDatabaseObject::load(value);
		break;
	default:
		break;
	}
	if (columnValue == nullptr) {
		string svalueType = (type == ColumnType::INT) ? "INT" : (type == ColumnType::DOUBLE) ? "DOUBLE" : (type == ColumnType::STRING) ? "String" : "NIL";
		out << "Incorrect value type. ColumnType is " << svalueType << endl;
	}

	return columnValue;
}

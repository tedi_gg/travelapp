#include <iostream>
#include <fstream>

#include "NilDatabaseObject.h"
#include "Table.h"
#include "../../util/File.h"
#include "IntegerDatabaseObject.h"
#include "DoubleDatabaseObject.h"
#include "StringDatabaseObject.h"


using namespace std;

void Table::copy(const Table& other)
{
	name = other.name;
	fileName = other.fileName;
	ts = new TableStructure();
	ts = other.ts;
}

void Table::remove()
{
	delete ts;

	for (vector<Row*>::iterator it = rows->begin(); it < rows->end(); ) {
		Row* row = *it;
		it = rows->erase(it);
		delete row;
	}
	delete rows;
}

Table::Table(string name, File* file,string fileName)
{
	this->name = name;
	this->f = file;
	this->fileName = fileName;
	ts = new TableStructure();
	rows = new vector<Row*>();
	
}

Table::Table(const Table& other)
{
	copy(other);
}

Table& Table::operator=(const Table& other)
{
	if (this != &other) {
		remove();
		copy(other);
	}
	return *this;
}

Table::~Table()
{
	remove();
}


VIterator<Row*>* Table::iterateRows()
{
	return new VIterator<Row*>(this->rows);
}

void Table::save(ofstream& out)
{
	save(this->name, out);
}

void Table::save(string name, ofstream& out)
{
	out << name << endl;

	int csize = ts->getColumnSize();
	for (int i = 0; i < csize; i++) {
		if (i > 0) {
			out << ',';
		}
		ColumnType type = ts->getColumnType(i);
		switch (type)
		{
		case ColumnType::NIL:
			cout << "Error: NilColumn" << endl;
			break;
		case ColumnType::INT:
			out << "I";
			break;
		case ColumnType::DOUBLE:
			out << "D";
			break;
		case ColumnType::STRING:
			out << "S";
			break;
		default:
			break;
		}
		string columnName = ts->getColumnName(i);
		out << columnName;
	}
	out << endl;

	for (vector<Row*>::iterator it = rows->begin(); it < rows->end(); it++) {
		Row* row = *it;
		for (int i = 0; i < csize; i++) {
			if (i != 0) {
				out << ",";
			}
			DatabaseObject* val = row->getColumn(i);
			if (val != nullptr) {
				val->save(out);
			}
		}
		out << endl;
	}

}

void Table::open()
{
	remove();
	ts = new TableStructure();
	rows = new vector<Row*>();

	ifstream* in = f->getInputStream();

	char* tableName = new char[4096];
	if (in->eof()) {
		cout << "Empty file: " << f->getFileName() << endl;
		in->close();
		return;
	}

	in->getline(tableName, 4096);

	string tmpName = tableName;
	if (tmpName != name) {
		cout << "Wrong table name, expected table name is " << name << " but in file it's " << tmpName << endl;
	}
	if (in->eof()) {
		cout << "Missing table structure" << name << endl;
		in->close();
		return;
	}

	char* buff = new char[4096];
	in->getline(buff, 4096);
	char* p = buff;
	int count =0;
	for (; *p != 0; p++) {
		ColumnType type;
		switch (*p)
		{
		case 'I':
			type = ColumnType::INT;
			break;
		case 'D':
			type = ColumnType::DOUBLE;
			break;
		case 'S':
			type = ColumnType::STRING;
			break;
		default:
			continue;
		}

		count++;

		p++;

		char* begin = p;
		for (; *p && (*p) != ','; p++);

		string name(begin, p - begin);
		Column* c = new Column(name, type);

		ts->addColumn(c);
		if (!(*p)) {
			p--; // The last element
		}
	}

	buff[0] = 0;
	
	string* line = nullptr; // extra line
	const char* sline;

	while (!in->eof()) {
		// Get line
		in->getline(buff, 4096);

		if (buff[0] == 0) {
			continue;
		}

		while ((in->fail() & in->failbit) && !in->eof()) {
			// Read line
			if (line == nullptr) {
				line = new string(buff);
			}
			else {
				line->append(buff);
			}
			in->getline(buff, 4096);
		}

		if (line != nullptr) {
			sline = line->data();
		}
		else {
			sline = buff;
		}


		// Process line
		int count = 0;
		Row* row = new Row();
		while (*sline) {
			// find sperator
			const char* p = sline;
			if (*p == '"') {
				p++;
				for (; *p && *p != '"'; p++) {
					if (*p == '\\') {
						if (*(p + 1) != 0) {
							p++;
						}
					}
				}
				if (*p) p++;

				// P should point comma (,)
			}
			else {
				for (; *p && *p != ','; p++); // Empty block
			}

			if (*p && *p != ',') {
				cout << "Wrong string format. Missing comma after final quote. The line will be skipped" << endl;
				delete row;
				row = nullptr;
				break;
			}

			if (ts->getColumnSize() <= count) {
				cout << "There is more column then definted int structure: " << ts->getColumnSize() << endl;
				break;
			}

			string* sColumnValue = (*p) ? new string(sline, p - sline) : new string(sline);
			ColumnType type = ts->getColumnType(count);
			DatabaseObject* columnValue = nullptr;
			switch (type)
			{
			case ColumnType::NIL:
				break;
			case ColumnType::INT:
				columnValue = IntegerDatabaseObject::load(*sColumnValue);
				break;
			case ColumnType::DOUBLE:
				columnValue = DoubleDatabaseObject::load(*sColumnValue);
				break;
			case ColumnType::STRING:
				columnValue = StringDatabaseObject::load(*sColumnValue);
				break;
			default:
				break;
			}

			if (columnValue == nullptr) {
				columnValue = NilDatabaseObject::getInstance();
			}

			row->addColumn(columnValue);

			sline = p;
			if (*sline) sline++;
			count++;
		}

		addRow(row);
		
		if (line != nullptr) {
			delete line;
			line = nullptr;
		}
	}

	in->close();
}

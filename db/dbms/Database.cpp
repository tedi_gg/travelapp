#include "Database.h"

using namespace std;

void Database::copy(const Database& other)
{

	tables = new map<string, Table*>();
	
	for (map<string, Table*>::iterator it = other.tables->begin(); it.operator!=(other.tables->end()); it++) {
		Table* t = it->second;
		addTable(new Table(*t));
	}
	
	orderedTable = new vector<Table*>();
	for (vector<Table*>::iterator it = other.orderedTable->begin(); it < other.orderedTable->end(); it++) {
		Table* t = *it;
		addTable(new Table(*t));
	}
}

void Database::remove()
{

	for (vector<Table*>::iterator it = orderedTable->begin(); it < orderedTable->end(); ) {
		Table* t = *it;
		it = orderedTable->erase(it);
		delete t;
	}
	
	delete orderedTable;
	delete tables;
}

Database::Database()
{
	tables = new map<string, Table*>();
	orderedTable = new vector<Table*>();
}

Database::Database(const Database& other)
{
	copy(other);
}

Database& Database::operator=(const Database& other)
{
	if (this != &other) {
		remove();
		copy(other);
	}
	return *this;
}

Database::~Database()
{
	remove();
}

bool Database::addTable(Table* t)
{
	string name = t->getName();
	tables->insert(std::pair<string, Table*>(name, t));
	if ((*tables)[name] == nullptr) {
		(*tables)[name] = t;
	}
	orderedTable->push_back(t);

	return true;
}

Table* Database::getTable(const string& name) const 
{
	return (*tables)[name];
}

VIterator<Table*>* Database::iterateTables()
{
	return new VIterator<Table*>(orderedTable->begin(), orderedTable->end());
}



void Database::save(File& file)
{
	ofstream *out = file.getOutputStream();
	
	string name;
	string filePath;

	for (VIterator<Table*>* it = iterateTables(); it->hasNext();) {
		Table* t = it->next();
		name = t->getName();
		filePath = t->getFileName();
		*out << name << "," << filePath << endl;
	}

	out->close();

	for (VIterator<Table*>* it = iterateTables(); it->hasNext();) {
		Table* t = it->next();
		filePath = t->getFileName();

		string file_path;

#ifdef _WIN32
		if (filePath[0] == '\\' || filePath.size() > 3 && filePath[0] >= 'A' && filePath[0] <= 'Z' && filePath[1] == ':' && filePath[2] == '\\') {
			file_path = filePath;
		}
		else {
			string baseFileName = file.getFileFullName();
			std::size_t found = baseFileName.find_last_of("\\");
			if (found == string::npos) {
				file_path = filePath;
			}
			else {
				file_path = baseFileName.substr(0, found) + filePath;
			}
		}
#else
		if (filePath[0] == '/') {
			file_path = filePath;
		}
		else {
			string baseFileName = file.getFileFullName();
			std::size_t found = baseFileName.find_last_of("/");
			if (found == string::npos) {
				file_path = filePath;
			}
			else {
				file_path = baseFileName.substr(0, found) + filePath;
			}
		}
#endif

		File* f = new File(file_path);
		t->save(*f->getOutputStream());
	}

}

Table* Database::rename(string& name, string& newName)
{
	Table* t = (*tables)[name];
	if (t != nullptr && (*tables)[newName] == nullptr) {
		(*tables)[name] = nullptr;
		(*tables)[newName] = t;
		t->setName(newName);
		return t;
	}

	return nullptr;
}

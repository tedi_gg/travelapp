#pragma once

#include "DatabaseObject.h"
class DoubleDatabaseObject : public DatabaseObject {
	const double data;

	string encoding();

protected:
	int cmp(const DatabaseObject& dbObj1) const;

public:
	DoubleDatabaseObject(const double aData) : data(aData) {}

	ColumnType getType() const { return ColumnType::DOUBLE; }
	void save(ostream&);

	string toString();

	static DoubleDatabaseObject* load(string& str);
	double getData() const  {
		return data;
	}
	DatabaseObject* clone() {
		return new DoubleDatabaseObject(data);
	}
};

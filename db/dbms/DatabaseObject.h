#pragma once

#include <iostream>
#include "ColumnType.h"

using namespace std;


class DatabaseObject {
private:

protected:
	virtual int cmp(const DatabaseObject& dbObj2) const = 0;
public:
	DatabaseObject() {}
	
	friend bool operator==(const DatabaseObject& dbObj1, const DatabaseObject& dbObj2) {
		return dbObj1.cmp(dbObj2) == 0; 
	}
	friend bool operator!=(const DatabaseObject& dbObj1, const DatabaseObject& dbObj2) {
		return dbObj1.cmp(dbObj2) != 0;
	}

	virtual ColumnType getType() const = 0;
	virtual void save(ostream&) = 0; // ostream


	virtual string toString() = 0; // 
	virtual DatabaseObject* clone() = 0;

};
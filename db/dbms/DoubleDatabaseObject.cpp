#include "DoubleDatabaseObject.h"
#include <iostream>  
#include <string> 
using namespace std;

string DoubleDatabaseObject::encoding()
{
	return to_string(data);
}
void DoubleDatabaseObject::save(ostream& out)
{
	out << encoding();
}

string DoubleDatabaseObject::toString()
{
	return encoding();
}

DoubleDatabaseObject* DoubleDatabaseObject::load(string& str)
{
	// Check for correct content.

	const char* data = str.data();
	if (!(*data)) {
		return nullptr;
	}

	if (*data == '-' || *data == '+') {
		data++;
	}

	// integer part - find dot
	while (*data && *data != '.') {
		if (*data <= '0' || *data > '9') {
			// Incorect contet
			return nullptr;
		}
		data++;
	}

	if (*data == '.') data++;

	// fraction
	while (*data) {
		if (*data <= '0' || *data > '9') {
			// Incorect contet
			return nullptr;
		}
		data++;
	}


	double val = atof(str.data());
	return new DoubleDatabaseObject(val);
}

int DoubleDatabaseObject::cmp(const DatabaseObject& dbObj2) const
{
	ColumnType type = dbObj2.getType();
	if (type == ColumnType::DOUBLE) {
		DoubleDatabaseObject* p = (DoubleDatabaseObject*)&dbObj2;
		double otherData = p->getData();
		return (this->data < otherData) ? -1 : (this->data == otherData) ? 0 : 1;	// In double is not using ==. We must use abs(a - b) < epsilon instead a == b.
	}

	if (type == ColumnType::STRING) {
		return -1;
	}
	return 1;
}

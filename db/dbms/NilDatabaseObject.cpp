#include "NilDatabaseObject.h"

NilDatabaseObject* NilDatabaseObject::instance = new NilDatabaseObject();


int NilDatabaseObject::cmp(const DatabaseObject& dbObj2) const
{
	ColumnType type = dbObj2.getType();
	if (type == ColumnType::NIL) {
		return 0;
	}

	return -1;
}

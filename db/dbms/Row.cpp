
#include "Row.h"

void Row::copy(const Row& other)
{
	dbObjects = new vector<DatabaseObject*>();
	for (vector<DatabaseObject*>::iterator it = other.dbObjects->begin(); it < other.dbObjects->end(); it++) {
		dbObjects->push_back((*it)->clone());
	}
}

void Row::remove()
{
	for (vector<DatabaseObject*>::iterator it = dbObjects->begin(); it < dbObjects->end(); ) {
		DatabaseObject* val = *it;
		if (val != nullptr && val->getType() != ColumnType::NIL) {
			delete val;
		}
		it = dbObjects->erase(it);
	}
	delete dbObjects;
}

Row::Row(const Row& other)
{
	copy(other);
}

Row& Row::operator=(const Row& other)
{
	if (this != &other) {
		remove();
		copy(other);
	}
	return *this;
}

Row::~Row()
{
	remove();
}

void Row::setColumn(int column, DatabaseObject* val)
{
	if (column >= 0 && column < dbObjects->size()) {
		DatabaseObject* oldVal = (*dbObjects)[column];
		if (oldVal != val && oldVal->getType() != ColumnType::NIL) {
			delete oldVal;
		}

		(*dbObjects)[column] = val;
	}
}

string Row::to_String()
{
	string line;
	bool isFirst = true;
	for (VIterator<DatabaseObject*>* it = iterateColumns(); it->hasNext();) {
		DatabaseObject* dbO = it->next();
		if (isFirst) {
			isFirst = false;
		}
		else {
			line += " | ";
		}
		line = dbO->toString();
	}

	return string();
}

VIterator<DatabaseObject*>* Row::iterateColumns()
{
	return new VIterator<DatabaseObject*>(this->dbObjects);
}


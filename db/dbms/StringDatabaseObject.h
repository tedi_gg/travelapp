#pragma once
#include <string>
#include "DatabaseObject.h"

class StringDatabaseObject : public DatabaseObject {
	string data;

	string encoding();

protected:
	int cmp(const DatabaseObject& dbObj1) const;

public:
	StringDatabaseObject(const string aData) : data(aData) {}

	ColumnType getType() const { return ColumnType::STRING; }
	void save(ostream& );

	string toString();
	string getData() const  {
		return data;
	}

	static StringDatabaseObject* load(string& str);

	DatabaseObject* clone() {
		return new StringDatabaseObject(data);
	}
};

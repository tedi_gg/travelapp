#pragma once

#include <string>
#include "ColumnType.h"

using namespace std;

class Column {
	string name;
	ColumnType type;


public:
	Column(string n, ColumnType type) : name(n), type(type) {}

	string getName() const  { return name; }
	ColumnType getType() const  { return type; }
	
};

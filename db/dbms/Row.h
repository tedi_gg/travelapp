#pragma once

#include <vector>

#include "DatabaseObject.h"
#include "../../util/VIterator.h"

using namespace std;

class Row {
	vector<DatabaseObject*>* dbObjects; // aray of objects - columns in the row

	void copy(const Row&);
	void remove();
public:
	Row() { dbObjects = new vector<DatabaseObject*>(); }
	Row(const Row&);
	Row& operator=(const Row&);
	~Row();

	bool addColumn(DatabaseObject* col) { dbObjects->push_back(col); return true; }
	DatabaseObject* getColumn(const int column) const  { return (column >= 0 && column < dbObjects->size()) ? (*dbObjects)[column] : nullptr; }
	void setColumn(int column, DatabaseObject* val);
	int size() { return dbObjects->size(); }
	string to_String();
	VIterator<DatabaseObject*>* iterateColumns();

};

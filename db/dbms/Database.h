#pragma once

#include <map>
#include <string>

#include "Table.h"
#include"../../util/VIterator.h"

using namespace std;

class Database {
private:
	map<string, Table*>* tables;
	vector<Table*>* orderedTable;

	void copy(const Database&);
	void remove();
public:
	Database();
	Database(const Database&);
	Database& operator=(const Database&);
	~Database();

	bool addTable(Table*);
	Table* getTable(const string& name) const ;
	VIterator<Table*>* iterateTables();
	void save(File&);
	Table* rename(string& name, string& newName);
};

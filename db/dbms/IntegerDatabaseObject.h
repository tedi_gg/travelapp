#pragma once

#include "DatabaseObject.h"

class IntegerDatabaseObject : public DatabaseObject {
	const long data;

	string encoding();

protected:
	int cmp(const DatabaseObject& dbObj1) const;

public:
	IntegerDatabaseObject(const long aData) : data(aData) {}

	ColumnType getType() const { return ColumnType::INT; }
	void save(ostream&);

	string toString();
	long getData() const  {
		return data;
	}

	static IntegerDatabaseObject* load(string& str);

	DatabaseObject* clone() {
		return new IntegerDatabaseObject(data);
	}
};

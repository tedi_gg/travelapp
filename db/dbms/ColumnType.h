#pragma once

enum class ColumnType{
	NIL = 0,
	INT = 1,
	DOUBLE = 2,
	STRING = 3
};
#pragma once

#include <vector>

#include "Column.h"
#include "../../util/VIterator.h"

using namespace std;

class TableStructure {
private:
	// columns
	vector<Column*>* columns;

	void cpy(const TableStructure&);
	void rmv();
public:
	TableStructure() { this->columns = new vector<Column*>(); }
	TableStructure(const TableStructure& );
	TableStructure& operator=(const TableStructure&);
	~TableStructure();
	
	int getColumnSize() const  { return columns->size(); }
	Column* getColumn(const int index) const  { return (*columns)[index]; }
	ColumnType getColumnType(const int index) const  { return (*columns)[index]->getType(); }
	string getColumnName(const int index) const  { return (*columns)[index]->getName(); }
	int getColumnIndex(const string name) const  {
		int index = 0;
		for (vector<Column*>::iterator it = columns->begin(); it != columns->end(); it++, index++) {
			if (name == (*it)->getName()) {
				return index;
			}

		}

		return -1;

	}

	bool addColumn(Column* column) {
		if (column->getType() != ColumnType::NIL) { columns->push_back(column); return true; }
		else  return false;
	}
	VIterator<Column*>* iterator() { return new VIterator<Column*>(columns->begin(), columns->end()); }

};

